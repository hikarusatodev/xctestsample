//
//  DetailViewController.swift
//  XCTestSample
//
//  Created by HikaruSato on 2016/03/10.
//  Copyright © 2016年 HikaruSato. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var starButton: PPParticleButton!
    
    @IBOutlet weak var loveButton: PPParticleButton!

    
    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.valueForKey("timeStamp")!.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
        
        self.starButton.particleFileNameMap[PPParticleButtonEffectType.Normal] = "StarParticle"
        self.loveButton.particleFileNameMap[PPParticleButtonEffectType.Normal] = "HeartParticle"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tapStar(sender: AnyObject) {
        self.setDescriptionLabelTextWithAnimation("Twinkle stars!")
    }
    
    @IBAction func tapLove(sender: AnyObject) {
        self.setDescriptionLabelTextWithAnimation("Many hearts!")
    }
    
    func setDescriptionLabelTextWithAnimation(text: String) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.detailDescriptionLabel.alpha = 0
            }, completion: { (_) -> Void in
                self.detailDescriptionLabel.alpha = 1
                self.detailDescriptionLabel.text = text
        })
    }

}

