//
//  XCTestSampleUITests.swift
//  XCTestSampleUITests
//
//  Created by HikaruSato on 2016/03/10.
//  Copyright © 2016年 HikaruSato. All rights reserved.
//

import XCTest

class XCTestSampleUITests: XCTestCase {
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectation = self.expectationWithDescription("test")
        let app = XCUIApplication()
        app.tables.staticTexts["2016-03-22 13:34:34 +0000"].tap()
        
        app.buttons["star"].tap()
        var delay = 1.0 * Double(NSEC_PER_SEC)
		var time  = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
		dispatch_after(time, dispatch_get_main_queue(), {
            XCTAssert(app.staticTexts["Twinkle stars!"].exists, "failed to tap star button.")
            app.buttons["love"].tap()
		})
        
        
        delay = 2.0 * Double(NSEC_PER_SEC)
        time  = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            XCTAssert(app.staticTexts["Many hearts!"].exists, "failed to tap love button.")
            expectation.fulfill()
        })
        
        
        self.waitForExpectationsWithTimeout(5.0) { (error) -> Void in
            if let err = error {
                XCTFail(err.description)
            }
            
        }
    }
}
